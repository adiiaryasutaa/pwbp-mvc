<?php

class HomeController extends Controller
{
	public function index()
	{
		redirect_unless(auth()->hasUser(), url('home/login'));

		$data['title'] = 'Home';
		$data['users'] = $this->model('User')
			->select(['id', 'username', 'email', 'first_name', 'last_name'])
			->all();

		if (count($data['users'])) {
			$data['table_users_columns'] = array_keys($data['users'][0]);
		}

		$this->view('templates/header', $data);
		$this->view('home/index', $data);
		$this->view('templates/footer', $data);
	}

	public function about($company = 'SMKN1')
	{
		// Anggap aja middleware
		redirect_unless(auth()->hasUser(), url('home/login'));

		$data['title'] = 'About';
		$data['company'] = $company;

		$this->view('templates/header', $data);
		$this->view('home/about', $data);
		$this->view('templates/footer', $data);
	}

	public function profile()
	{
		redirect_unless(auth()->hasUser(), url('home/login'));

		$data['title'] = 'Profile';
		$data['user'] = auth()->user();

		$this->view('templates/header', $data);
		$this->view('home/profile', $data);
		$this->view('templates/footer', $data);
	}

	public function register()
	{
		redirect_if(auth()->hasUser(), url('home/index'));

		$data['title'] = 'Register';

		$this->view('auth/register', $data);
	}

	public function store()
	{
		redirect_if(auth()->hasUser(), url('home/index'));

		$data = [
			'email' => $_POST['email'],
			'username' => $_POST['username'],
			'first_name' => $_POST['first-name'],
			'last_name' => $_POST['last-name'],
			'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
		];

		$created = $this->model('User')->create($data);

		if ($created) {
			Flasher::setFlash('Register success', 'sekarang Anda dapat masuk', 'success');
			redirect(url('home/login'), true);
		}

		Flasher::setFlash('Register failed', 'coba lagi', 'danger');
		return back();
	}

	public function login($data = [])
	{
		redirect_if(auth()->hasUser(), url('home/index'));

		$data['title'] = 'Login';

		$this->view('auth/login', $data);
	}

	public function authenticate()
	{
		redirect_if(auth()->hasUser(), url('home/index'));

		$credentials = [
			'email' => $_POST['email'],
			'password' => $_POST['password'],
		];

		$loggedIn = auth()->attempt($credentials);

		if ($loggedIn) {
			return redirect(url('home/index'), true);
		}

		Flasher::setFlash('Login gagal', 'coba lagi', 'danger');

		return back();
	}

	public function logout()
	{
		auth()->logout();

		return back();
	}
}
