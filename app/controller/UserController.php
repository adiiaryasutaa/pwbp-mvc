<?php

class UserController extends Controller
{
	public function create()
	{
		$data['title'] = 'Create user';

		$this->viewWithLayout('user/create', $data);
	}

	public function store()
	{
		$validated = $this->validation('User')->validate([
			'username' => $_POST['username'],
			'email' => $_POST['email'],
			'first_name' => $_POST['first-name'],
			'last_name' => $_POST['last-name'],
			'password' => md5($_POST['password'] . SALT),
		]);

		return $this->model('UserModel')->create($validated) ?
			redirect(url('home/index'), true) :
			back();
	}
}
