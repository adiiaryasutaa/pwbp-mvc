<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.101.0">
	<title><?= $data['title'] ?></title>

	<link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">

	<link href="<?= url('css/bootstrap.min.css') ?>" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}

		.b-example-divider {
			height: 3rem;
			background-color: rgba(0, 0, 0, .1);
			border: solid rgba(0, 0, 0, .15);
			border-width: 1px 0;
			box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
		}

		.b-example-vr {
			flex-shrink: 0;
			width: 1.5rem;
			height: 100vh;
		}

		.bi {
			vertical-align: -.125em;
			fill: currentColor;
		}

		.nav-scroller {
			position: relative;
			z-index: 2;
			height: 2.75rem;
			overflow-y: hidden;
		}

		.nav-scroller .nav {
			display: flex;
			flex-wrap: nowrap;
			padding-bottom: 1rem;
			margin-top: -1px;
			overflow-x: auto;
			text-align: center;
			white-space: nowrap;
			-webkit-overflow-scrolling: touch;
		}
	</style>


	<!-- Custom styles for this template -->
	<link href="<?= url('css/register.css') ?>" rel="stylesheet">
</head>

<body class="text-center">

	<main class="form-signin w-100 m-auto">
		<form action="<?= url('home/store') ?>" method="POST">
			<img class="mb-4" src="<?= url('image/bootstrap-logo.svg') ?>" alt="brand" width="72" height="57">

			<h1 class="h3 mb-3 fw-normal">Please Sign Up</h1>

			<div class="form-floating">
				<input name="email" type="email" class="form-control rounded-top" id="email" placeholder="email@mail.test">
				<label for="email">Email address</label>
			</div>
			<div class="form-floating">
				<input name="username" type="text" class="form-control" id="username" placeholder="Hehe">
				<label for="username">Username</label>
			</div>
			<div class="form-floating">
				<input name="password" type="password" class="form-control" id="password" placeholder="password">
				<label for="password">Password</label>
			</div>
			<div class="form-floating">
				<input name="verify-password" type="password" class="form-control" id="verify-password" placeholder="password">
				<label for="verify-password">Verify password</label>
			</div>
			<div class="form-floating">
				<input name="first-name" type="text" class="form-control" id="first-name" placeholder="firstname">
				<label for="first-name">First name</label>
			</div>
			<div class="form-floating mb-3">
				<input name="last-name" type="text" class="form-control rounded-bottom" id="last-name" placeholder="lastname">
				<label for="last-name">Last name</label>
			</div>

			<button class="w-100 btn btn-lg btn-primary" name="submit" type="submit">Sign Up</button>
			<p class="mt-5 mb-3 text-muted">&copy; 2022</p>
		</form>
	</main>

</body>

</html>