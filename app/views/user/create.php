<main>
	<div class="container d-flex flex-column align-items-center mt-5">
		<h2 class="mb-5">Register</h2>
		<form class="w-25" action="<?= url('user/store') ?>" method="POST">
			<div class="mb-3">
				<label for="email" class="form-label">Email address</label>
				<input name="email" type="email" class="form-control" id="email" placeholder="name@example.com">
			</div>
			<div class="mb-3">
				<label for="username" class="form-label">Username</label>
				<input name="username" type="text" class="form-control" id="username" placeholder="begalkantin123">
			</div>
			<div class="mb-3">
				<label class="mb-2">First and last name</label>
				<div class="input-group">
					<input name="first-name" type="text" aria-label="First name" class="form-control" placeholder="Raiden">
					<input name="last-name" type="text" aria-label="Last name" class="form-control" placeholder="Shogun">
				</div>
			</div>
			<div class="mb-3">
				<label for="password" class="form-label">Password</label>
				<input name="password" type="password" class="form-control" id="password">
			</div>
			<div class="mb-5">
				<label for="confirm-password" class="form-label">Confirm password</label>
				<input name="confirm-password" type="password" class="form-control" id="password">
			</div>
			<div>
				<button class="btn btn-primary w-100" type="submit" name="submit">Submit</button>
			</div>
		</form>
	</div>
</main>