<?php

use App\Core\Helper\Arr;
use App\Core\Interface\SessionInterface;

class Session implements SessionInterface
{
	public function __construct()
	{
		if (session_status() === PHP_SESSION_NONE) {
			session_start();
		}
	}

	public function get($key, $default = null)
	{
		if ($this->has($key)) {
			return $_SESSION[$key];
		}

		return $default;
	}

	public function pull($key, $default = null)
	{
		$value = $this->get($key, $default);

		$this->remove($key);

		return $value;
	}

	public function set($key, $value)
	{
		$_SESSION[$key] = $value;

		return $this;
	}

	public function remove($key)
	{
		if ($this->has($key)) {
			unset($_SESSION[$key]);
		}
	}

	public function clear()
	{
		return session_unset();
	}

	public function has($key)
	{
		return Arr::has($_SESSION, $key);
	}
}
