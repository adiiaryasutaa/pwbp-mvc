<?php

abstract class Controller
{
	public function view($view, $data = [])
	{
		require_once("../app/views/$view.php");
	}

	public function viewWithLayout($view, $data = [])
	{
		$this->view('layout/top', $data);
		$this->view($view, $data);
		$this->view('layout/bottom', $data);
	}

	public function model($model)
	{
		require_once("../app/model/$model.php");

		return new $model;
	}

	public function validation($validation)
	{
		require_once("../app/validation/$validation.php");

		return new $validation;
	}
}
