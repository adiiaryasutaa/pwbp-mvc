<?php

abstract class Model
{
	protected $database;
	protected $queryBuilder;
	protected $table;

	public function __construct($table)
	{
		$this->database = new Database();
		$this->table = $table;

		$this->queryBuilder = queryBuilder($this->table);
	}

	public function select(string|array $columns = ['*'])
	{
		$this->queryBuilder->select($columns);

		return $this;
	}

	public function where($column, $operator = null, $value = null, $boolean = 'AND')
	{
		$this->queryBuilder->where($column, $operator, $value, $boolean);

		return $this;
	}

	public function first()
	{
		return $this->database
			->query($this->queryBuilder->build())
			->bindAll($this->queryBuilder->getBindings())
			->result();
	}

	public function all()
	{
		return $this->database
			->query($this->queryBuilder->build())
			->bindAll($this->queryBuilder->getBindings())
			->results();
	}

	public function create($data = [])
	{
		$query = $this->queryBuilder->insert($data);

		return $this->database
			->query($query->build())
			->bindAll($query->getParams())
			->execute();
	}
}
