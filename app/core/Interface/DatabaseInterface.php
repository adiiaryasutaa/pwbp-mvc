<?php

namespace App\Core\Interface;

interface DatabaseInterface
{
	public function connect();

	public function query($query);

	public function bind($param, $value, $type);

	public function bindAll($bindings);

	public function execute();

	public function result();

	public function results();
}
