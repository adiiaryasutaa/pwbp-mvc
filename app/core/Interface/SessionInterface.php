<?php

namespace App\Core\Interface;

interface SessionInterface
{
	public function get($key, $default = null);

	public function set($key, $value);

	public function remove($key);

	public function clear();

	public function has($key);
}
