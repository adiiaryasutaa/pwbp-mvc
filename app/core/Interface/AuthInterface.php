<?php

namespace App\Core\Interface;

interface AuthInterface
{
	public function attempt($credentials, $remember);

	public function login($user);

	public function logout();

	public function user();
}
