<?php

use App\Core\Helper\Arr;
use App\Core\Interface\AuthInterface;

class Auth implements AuthInterface
{
	private Session $sessionManager;

	public function __construct()
	{
		$this->sessionManager = new Session();
	}

	public function attempt($credentials, $remember = false)
	{
		require_once('../app/model/User.php');

		$password = Arr::pull($credentials, 'password', '');

		$user = (new User())
			->select()
			->where('email', '=', $credentials['email'])->first();

		if (!empty($user) && password_verify($password, $user['password'])) {
			$this->login($user);

			return true;
		}

		return false;
	}

	public function login($user)
	{
		$this->sessionManager->set('user', $user);
	}

	public function logout()
	{
		$this->sessionManager->remove('user');
	}

	public function user()
	{
		return $this->sessionManager->get('user');
	}

	public function hasUser()
	{
		return !is_null($this->user());
	}
}
