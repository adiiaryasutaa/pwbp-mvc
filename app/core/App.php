<?php

use App\Core\Helper\Arr;

class App
{
	protected $controller = "Home";
	protected $method = "index";
	protected $params = [];

	public function __construct()
	{
		$url = $this->parseUrl();

		$controller = Arr::pull($url, 0, 'home');
		$this->findAndInstantiateController($controller);

		$method = Arr::pull($url, 1, 'index');
		$this->findControllerMethod($method);

		$this->params = array_values($url);
		call_user_func_array([$this->controller, $this->method], $this->params);
	}

	private function parseUrl()
	{
		if (isset($_GET['url'])) {
			$url = $_GET['url'];

			if (str_contains($url, 'public')) {
				$url = str_replace('public/', '', $url);
			}

			$url = rtrim(
				filter_var($url, FILTER_SANITIZE_URL),
				'/'
			);

			return explode('/', $url);
		}

		return [];
	}

	private function findAndInstantiateController(string $name, ?string $default = null)
	{
		$className = "{$name}Controller";
		$path = Path::controllerPath($className);

		if (!file_exists($path)) {
			$className = is_null($default) ? "HomeController" : "{$default}Controller";
			$path = Path::controllerPath($className);
		}

		require_once($path);

		return $this->controller = new $className;
	}

	public function findControllerMethod(string $name, ?string $default = null)
	{
		if (!method_exists($this->controller, $name)) {
			$name = $default ?? 'index';
		}

		return $this->method = $name;
	}
}
