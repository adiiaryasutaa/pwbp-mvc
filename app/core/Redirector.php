<?php

class Redirector
{
	private $location = '';
	private $messages = [];

	private function go()
	{
		header("Location: {$this->location}");
		exit();
	}

	public function to(string $path)
	{
		$this->location = $path;

		return $this->go();
	}

	public function with(array $message = [])
	{
		$this->messages = $message;

		return $this;
	}
}
