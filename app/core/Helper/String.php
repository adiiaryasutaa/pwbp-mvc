<?php

namespace App\Core\Helper;

class Str
{
	/**
	 *
	 */
	public static function concatAll($string, $separator = '', ...$strings)
	{
		array_unshift($strings, $string);

		return self::join($strings, $separator);
	}

	/**
	 *
	 */
	public static function join($array, $separator = '')
	{
		return implode($separator, $array);
	}

	/**
	 *
	 */
	public static function joinKeyAndValue($array, $separator = '')
	{
		$strings = [];

		foreach ($array as $key => $value) {
			$strings[] = self::join([$key, $value], $separator);
		}

		return $strings;
	}

	public static function pdoBindingParam($field)
	{
		return ":$field";
	}

	/**
	 *
	 */
	public static function pdoBindingParams($array)
	{
		$bindings = [];

		foreach ($array as $binding) {
			$bindings[] = self::pdoBindingParam($binding);
		}

		return $bindings;
	}

	/**
	 *
	 */
	public static function placeholder($string, $placeholder, $value)
	{
		return str_ireplace($string, $placeholder, $value);
	}
}
