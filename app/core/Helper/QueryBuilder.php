<?php

use App\Core\Helper\Arr;
use App\Core\Helper\Str;

class QueryBuilder
{
	private $query = '';
	private $dml = '';
	private $table;
	private $columns = ['*'];
	private $bindings = [];
	private $wheres = [];
	private $params = [];

	private $preparedLogicalOperator = null;

	public function __construct(string $table)
	{
		$this->table = $table;
	}

	public function select(array $columns = ['*'])
	{
		$this->reset();

		$this->dml = 'SELECT';
		$this->columns = $columns;

		return $this;
	}

	public function insert($values = [])
	{
		$this->reset();

		$this->dml = 'INSERT';

		$this->columns = array_keys($values);
		$this->bindings = Str::pdoBindingParams($this->columns);
		$this->params = Arr::combine($this->bindings, array_values($values));

		return $this;
	}

	public function where($column, $operator = null, $value = null, $boolean = 'AND')
	{
		$param = $this->params[$column] = Str::pdoBindingParam($column);

		if (func_num_args() === 2) {
			$value = $operator;
			$operator = '=';
		}

		$this->wheres[] = Str::join([
			$this->preparedLogicalOperator ?? '',
			$column,
			$operator ?? '=',
			$param,
		], ' ');

		$this->bindings[$param] = $value;

		$this->preparedLogicalOperator = $boolean;

		return $this;
	}

	public function getParams()
	{
		return $this->params;
	}

	public function build()
	{
		match ($this->dml) {
			'SELECT' => $this->buildSelectQuery(),
			'INSERT' => $this->buildInsertQuery(),
		};

		return $this->query;
	}

	public function dd()
	{
		$this->build();

		dd($this->query);
	}

	public function reset()
	{
		$this->dml = '';
		$this->fields = [];
		$this->tables = [];
		$this->conditions = [];
		$this->query = '';

		return $this;
	}

	private function buildSelectQuery()
	{
		$this->query = Str::join([
			'SELECT',
			Str::join($this->columns, ', '),
			'FROM',
			$this->table
		], ' ');

		if (!empty($this->wheres)) {
			$this->query = Str::join([
				$this->query,
				'WHERE',
				Str::join($this->wheres, ' ')
			], ' ');
		}
	}

	private function buildInsertQuery()
	{
		$this->query = Str::join([
			'INSERT INTO',
			$this->table,
			'(',
			Str::join($this->columns, ', '),
			') VALUES (',
			Str::join($this->bindings, ', '),
			')'
		], ' ');
	}

	public function getBindings()
	{
		return $this->bindings;
	}
}
