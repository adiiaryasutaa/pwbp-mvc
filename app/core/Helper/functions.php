<?php

/**
 *
 */
function dd($value)
{
	var_dump($value);
	die;
}

/**
 *
 */
function url(string $path)
{
	return BASE_URL . "/$path";
}

/**
 *
 */
function currentUrl()
{
	$url = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
		'https://' : 'http://';

	$url .= $_SERVER['HTTP_HOST'];

	$url .= $_SERVER['REQUEST_URI'];

	return $url;
}

/**
 *
 */
function redirect($path, $permanent = false)
{
	return (new Redirector())->to($path);
}

/**
 *
 */
function redirect_if($condition, $path, $permanent = false)
{
	if ($condition) {
		redirect($path, $permanent);
	}
}

/**
 *
 */
function redirect_unless($condition, $path, $permanent = false)
{
	redirect_if(!$condition, $path, $permanent);
}

function session(): Session
{
	return new Session();
}

function back($message = [])
{
	foreach ($message as $key => $value) {
		session()->set($key, $value);
	}

	return redirect($_SERVER['HTTP_REFERER']);
}
