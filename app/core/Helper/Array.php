<?php

namespace App\Core\Helper;

class Arr
{
	public static function set($array, array|string $values)
	{
		if (is_string($values)) {
			foreach ($array as $key => $value) {
				$array[$key] = $values;
			}

			return $array;
		}

		$i = 0;
		$lastValueIndex = count($values) - 1;
		foreach ($array as $key => $value) {
			$array[$key] = $values[$i];

			if ($i <= $lastValueIndex) {
				$i++;
			}
		}

		return $array;
	}

	public static function combine($keys, $values)
	{
		return array_combine($keys, $values);
	}

	public static function pull(&$array, $key, $default = null)
	{
		if (self::has($array, $key)) {
			$a = $array[$key];

			unset($array[$key]);

			return $a;
		}

		return $default;
	}

	public static function has($array, $key)
	{
		return array_key_exists($key, $array);
	}
}
