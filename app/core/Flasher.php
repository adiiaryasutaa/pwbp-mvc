<?php

class Flasher
{
	public static function setFlash($messsage, $action, $type)
	{
		session()->set(
			'flash',
			[
				'message' => $messsage,
				'action' => $action,
				'type' => $type,
			],
		);
	}

	public static function flash()
	{
		$flash = session()->pull('flash');

		if (is_null($flash)) {
			return;
		}

		echo '
			<div class="alert alert-' . $flash['type'] . ' alert-dismissable fade show" role="alert">
				<strong> ' . $flash['message'] . ' </strong> ' . $flash['action'] . '
				<button type="button" class="btn-close" data-dismiss="alert" aria-label="Close"></button>
			</div>
		';
	}
}
